import axios from 'axios';
import { GATEWAY_URI } from '../config';
import {Cookies} from "react-cookie";
import { useHistory } from 'react-router';

const AxiosInstance = axios.create({
  baseURL: GATEWAY_URI,
  headers: {
    'Content-Type': 'application/json'
  }
});

AxiosInstance.interceptors.request.use(
  (config) => {
    return config;
  },
  (error) => Promise.reject(error)
);

AxiosInstance.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    // const originalRequest = error.config;
    if (!error.response) {
      return Promise.reject('Network Error');
    }

    if (error.response.status === 401) {
      const history = useHistory();
      history.push('/sigin');
      return Promise.reject('Unathorization');
    }

    // else if ((error.response.status === 401) && !originalRequest._retry) {
    //   originalRequest._retry = true;
    //   return AuthService.getToken()
    //     .then(token => {
    //       const authTokenResponse = path(['data', 'response'], token)
    //       AxiosInstance.defaults.headers.common['Authorization'] = 'Bearer ' + authTokenResponse;
    //       originalRequest.headers['Authorization'] = 'Bearer ' + authTokenResponse;
    //       return axios(originalRequest);
    //     })
    //     .catch(err => err)
    // } else {
    //   return error.response
    // }
    return Promise.reject(error.response);
  }
);

export default AxiosInstance;
