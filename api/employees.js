import internals from '../util/httpClient';

export const getAll = (options) => internals.get('/users',[],{...options});
export const create = (payload,options) => internals.post('/users/create',payload,{...options});
export const get = (payload,options) => internals.get('/users/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/users/update',payload,{...options});
export const remove = (payload,options) => internals.post('/users/delete',payload,{...options});