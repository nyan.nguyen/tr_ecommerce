import internals from '../util/httpClient';

export const getAll = (options) => internals.get('/tax',[],{...options});
export const create = (payload,options) => internals.post('/tax/create',payload,{...options});
export const get = (payload,options) => internals.get('/tax/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/tax/update',payload,{...options});
export const remove = (payload,options) => internals.post('/tax/delete',payload,{...options});