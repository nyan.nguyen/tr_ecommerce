import internals from '../util/httpClient';

export const getAll = (options) => internals.get('/variation',[],{...options});
export const create = (payload,options) => internals.post('/variation/create',payload,{...options});
export const get = (payload,options) => internals.get('/variation/get/'+payload._id,[],{...options});
export const update = (payload,options) => internals.post('/variation/update',payload,{...options});
export const remove = (payload,options) => internals.post('/variation/delete',payload,{...options});