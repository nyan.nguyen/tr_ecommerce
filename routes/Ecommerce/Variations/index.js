import React, { useEffect } from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { notification, Popconfirm, Space, Table } from 'antd';
import moment from 'moment';
import { VariationsApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const Variations = ({props}) => {
	const {variations} = props;
    const [cookie] = useCookies();
	const router = useRouter();
	const intl = useIntl();

	const onDelete = (_id) => {
		VariationsApi.remove({
			_id: _id
		}, {
			headers: {
				"x-auth-token" :cookie.token
			}
		}).then(response => {
			if(response.errors) {
				console.log(response.errors);
				notification['error']({
					message: intl.formatMessage({id: "error.occured"})
				})
			} else {
				notification['success']({
					message: intl.formatMessage({id: "variation.delete.success"})
				});
				router.replace(router.asPath);
			}
		})
	}

	const columns = [
		{
            title: <IntlMessages id="table.column.name"/>,
            dataIndex: 'name',
            key: 'name',
        },
		{
			title: <IntlMessages id="table.column.action"/>,
			key: 'action',
			render: (text, record) => (
			  <Space size="middle">
				<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/edit-variation/${record._id}`)} href="#"><IntlMessages id="button.modify" /></a>
				<Popconfirm
					title={<IntlMessages id="confirm.delete.message" />}
					onConfirm={() => onDelete(record._id)}
					okText={<IntlMessages id="button.yes" />}
					cancelText={<IntlMessages id="button.cancel" />}
				>
					<a href="#"><IntlMessages id="button.delete" /></a>
				</Popconfirm>
			  </Space>
			),
		},
	]

	return (
		<Widget styleName="gx-card-profile" extra={<a onClick={() => router.push(`/${DASHBOARD_PATH}ecommerce/create-variation`)} href="#"><IntlMessages id="button.add"/></a>} title={<IntlMessages id="sidebar.ecommerce.variations"/>}>
            <Table className="gx-table-responsive" columns={columns} dataSource={variations}/>
        </Widget>
	);
};

export default Variations;
