import { Button, Col, Form, Input, InputNumber, notification, Row, Select, TreeSelect, Upload } from 'antd';
import React, { useEffect, useState } from 'react';
import Widget from '../../../app/components/Widget';
import IntlMessages from '../../../util/IntlMessages';
import { MinusCircleOutlined, PlusOutlined, InboxOutlined } from '@ant-design/icons';
import { useIntl } from 'react-intl';
import Helpers from '../../../util/Helpers';
import { ProductsApi } from '../../../api';
import { useCookies } from 'react-cookie';
import { useRouter } from 'next/router';
import { DASHBOARD_PATH } from '../../../config';

const ProductForm = ({props}) => {
    const intl = useIntl();
    const [coverPhoto, setCoverPhoto] = useState("");
    const [galery, setGalery] = useState([]);
    const [variationsSelectData, setVariationsSelectData] = useState([]);
    const [currentTaxSelected, setCurrentTaxSelected] = useState("");

    const [priceTaxExcluded, setPriceTaxExcluded] = useState(0);
    const [priceTaxIncluded, setPriceTaxIncluded] = useState(0);
    const [cookie] = useCookies();
    const router = useRouter();

    const { categories, taxes, variations, brands, suppliers, product } = props;
    
    console.log(categories, taxes, variations);

    useEffect(() => {
        if(product && Object.keys(product)){
            setGalery(product.galery);
            setCoverPhoto(product.cover);
        }
    },[product])

    const onSubmitForm = (values) => {
        let tax = currentTaxSelected?taxes.find(t => t._id === currentTaxSelected).value:0;
        console.log(values)
        console.log({
            ...product,
                ...values,
                cover: coverPhoto,
                galery: galery,
                price_tax_excluded: values.price_tax_included / (1+(tax/100)),
                variants: values.variants.stock?[]:values.variants, 
                stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
        });
        if(!product) {
            console.log("create")
            ProductsApi.create({
                ...values,
                cover: coverPhoto,
                galery: galery,
                price_tax_excluded: values.price_tax_included / (1+(tax/100)),
                variants: values.variants.stock?[]:values.variants,
                stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
            }, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                console.log(response)
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "product.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/products`)
                }
            })
        } else {
            console.log("update")
            ProductsApi.update({
                ...product,
                ...values,
                cover: coverPhoto,
                galery: galery,
                price_tax_excluded: values.price_tax_included / (1+(tax/100)),
                variants: values.variants.stock?[]:values.variants, 
                stock: values.variants.stock?values.variants.stock:values.variants.reduce(((a,b) => a+ parseInt(b.stock)),0)
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "product.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/products`)
                }
            })
        }
    }

    useEffect(() => {
        console.log(product)
        console.log(variations)
        if(variations.length) {
            setVariationsSelectData(
                variations.map(variation => ({
                    title: variation.name,
                    key: variation._id,
                    value: variation._id,
                    selectable: false,
                    checkable: false,
                    children: variation.values.length?variation.values.map(value => (
                        {
                            title: value.name,
                            value: variation._id+"#"+value._id,
                            key: variation._id+"#"+value._id
                        }
                    )):[]
                }))
            )
        }
    },[variations])

    const treeData = [
        {
          title: 'Node1',
          value: '0-0',
          key: '0-0',
          selectable: false,
          checkable: false,
          children: [
            {
              title: 'Child Node1',
              value: '0-0-0',
              key: '0-0-0',
            },
          ],
        },
        {
          title: 'Node2',
          value: '0-1',
          checkable: false,
          selectable: false,
          key: '0-1',
          children: [
            {
              title: 'Child Node3',
              value: '0-1-0',
              key: '0-1-0',
            },
            {
              title: 'Child Node4',
              value: '0-1-1',
              key: '0-1-1',
            },
            {
              title: 'Child Node5',
              value: '0-1-2',
              key: '0-1-2',
            },
          ],
        },
    ];

    const coverUploadProps = {
        name: 'cover',
        multiple: false,
        fileList: coverPhoto?[{
            uid: 'cover_photo',
            name: 'cover_photo',
            url: coverPhoto
        }]:[],
        listType:"picture",
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setCoverPhoto(base64);
            console.log(base64)
            // Prevent upload
            return true;
        },
        onRemove: (file) => {
            setCoverPhoto("");
        }
    };

    const galeryUploadProps = {
        name: 'galery',
        multiple: true,
        listType:"picture",
        fileList: galery.length?galery.map((galery,idx) => ({
            uid: idx,
            name: 'Galery Photo '+idx,
            url: galery
        })):[],
        beforeUpload : async(file) => {
            const base64 = await Helpers.toBase64(file);
            setGalery(oldGalery => [...oldGalery,base64]);
            // Prevent upload
            console.log(galery)
            return true;
        },
        onRemove: (file) => {
            const index = galery.findIndex((photo,idx) => file.name==="Galery Photo "+idx);
            let tmp = [...galery];
            tmp.splice(index, 1);
            setGalery(tmp);
        }
    };
  
      
	return (
        <Form
            layout="vertical" 
            onFinish={onSubmitForm}
            initialValues={{
                ...product
            }}
        >
            <Row style={{flexDirection: "row"}}>
                <Col xl={16} lg={14} md={14} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.product_information"/>} styleName="gx-card-profile">
                                <Form.Item name="name" label={<IntlMessages id="form.name"/>}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item name="short_description" label={<IntlMessages id="form.short_description"/>}>
                                    <Input.TextArea rows={4}/>
                                </Form.Item>
                                <Form.Item name="description" label={<IntlMessages id="form.description"/>}>
                                    <Input.TextArea rows={6}/>
                                </Form.Item>
                            </Widget>        
                        </Col>
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.inventory"/>} styleName="gx-card-profile">
                                <Form.List name="variants">
                                    {(fields, { add, remove }) => {
                                        return (
                                            <div>
                                                {fields.map((field,index) => (
                                                    <Row key={index} style={{flexDirection: "row"}}>
                                                        <Col xl={9} lg={11} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"attributeGroups"]} label={<IntlMessages id="product.form.attribute"/>}>
                                                                <TreeSelect 
                                                                    treeData={variationsSelectData}
                                                                    treeCheckable
                                                                />
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={4} lg={11} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"stock"]} label={<IntlMessages id="product.form.quantity"/>}>
                                                                <Input type="number"/>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={9} lg={11} md={24} sm={24} xs={24}>
                                                            <Form.Item name={[index,"images"]} label={<IntlMessages id="product.form.photo"/>}>
                                                                <Select 
                                                                    mode="multiple"
                                                                    allowClear
                                                                    showSearch
                                                                >
                                                                    {galery.map((photo,idx) => (
                                                                        <Select.Option key={idx} value={idx}>{`Galery Photo ${idx}`}</Select.Option>
                                                                    ))}
                                                                </Select>
                                                            </Form.Item>
                                                        </Col>
                                                        <Col xl={2} lg={2} md={24} sm={24} xs={24} style={{placeSelf:"center"}}>
                                                            <MinusCircleOutlined
                                                                className="dynamic-delete-button"
                                                                style={{ margin: '0 8px' }}
                                                                onClick={() => {
                                                                    remove(field.name);
                                                                }}
                                                            />
                                                        </Col>
                                                    </Row>
                                                ))}
                                                <Row>
                                                    <Col xl={fields.length>0?22:24} lg={fields.length>0?22:24} md={24} sm={24} xs={24}>
                                                        <Form.Item>
                                                            <Button
                                                                type="dashed"
                                                                onClick={() => {
                                                                    add();
                                                                }}
                                                                style={{ width: '100%' }}
                                                            >
                                                                <PlusOutlined /> <IntlMessages id="product.form.add_new_variation"/>
                                                            </Button>
                                                        </Form.Item>
                                                    </Col>
                                                </Row>
                                            </div>
                                        )
                                    }}
                                </Form.List>
                            </Widget>        
                        </Col>
                    
                        <Col xl={24} lg={24} md={24} sm={24} xs={24}>
                            <Widget title={<IntlMessages id="product.form.galery"/>}>
                                <Upload.Dragger {...galeryUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                            </Widget>
                        </Col>
                    </Row>
                </Col>
            
                <Col xl={8} lg={10} md={10} sm={24} xs={24}>
                    <Row>
                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.cover_photo"/>}>
                                <Upload.Dragger {...coverUploadProps}>
                                    <p className="ant-upload-drag-icon">
                                        <InboxOutlined />
                                    </p>
                                </Upload.Dragger>
                            </Widget>
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.price"/>}>
                                <Form.Item name="taxId" label={<IntlMessages id="form.tax"/>}>
                                    <Select showSearch onChange={(value) => setCurrentTaxSelected(value)}>
                                        {taxes.map(tax => (
                                            <Select.Option key={tax._id} value={tax._id}>{tax.name} ({tax.value}%)</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="price_tax_included" label={<IntlMessages id="form.price_tax_included"/>}>
                                    <Input 
                                        style={{width: "100%"}}       
                                        suffix="VND"
                                    />
                                </Form.Item>
                            </Widget>
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Widget title={<IntlMessages id="product.form.reference"/>}>
                                <Form.Item name="sku" label={<IntlMessages id="form.sku"/>}>
                                    <Input/>
                                </Form.Item>
                                <Form.Item name="brandId" label={<IntlMessages id="form.brand"/>}>
                                    <Select 
                                        allowClear
                                        showSearch
                                    >
                                        {brands.map(brand => (
                                            <Select.Option key={brand._id} value={brand._id}>{brand.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="supplyId" label={<IntlMessages id="form.supplier"/>}>
                                    <Select 
                                        allowClear
                                        showSearch
                                    >
                                        {suppliers.map(supplier => (
                                            <Select.Option key={supplier._id} value={supplier._id}>{supplier.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                                <Form.Item name="categoryId" label={<IntlMessages id="form.category"/>}>
                                    <Select 
                                        mode="multiple"
                                        allowClear
                                        showSearch
                                    >
                                        {categories.map(category => (
                                            <Select.Option key={category._id} value={category._id}>{category.name}</Select.Option>
                                        ))}
                                    </Select>
                                </Form.Item>
                            </Widget>
                        </Col>

                        <Col xl={24} lg={24} md={24} sm={12} xs={24}>
                            <Form.Item>
                                <Button type="primary" htmlType="submit" style={{width: "100%"}}>
                                    <IntlMessages id="button.submit"/> 
                                </Button>
                            </Form.Item>
                        </Col>
                    </Row>
                </Col>
            
            </Row>
        </Form>    
	);
};

export default ProductForm;
