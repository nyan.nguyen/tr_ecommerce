import React from 'react';
import Widget from "../../../app/components/Widget";
import IntlMessages from '../../../util/IntlMessages';
import { Button, Col, Form, Input, notification, Row } from 'antd';
import { useCookies } from 'react-cookie';
import { CategoriesApi } from '../../../api';
import { useRouter } from 'next/router';
import { useIntl } from 'react-intl';
import { DASHBOARD_PATH } from '../../../config';

const CategoryForm = ({props}) => {
    const category = props?.category;
    const intl = useIntl();
    const [cookie] = useCookies();
    const router = useRouter();
    
    const onSubmitForm = (values) => {
        if(!category) {
            CategoriesApi.create(values, {
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "category.create.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/categories`)
                }
            })
        } else {
            CategoriesApi.update({
                ...category,
                ...values
            },{
                headers: {
                    "x-auth-token" :cookie.token
                }
            })
            .then(response => {
                if(response.errors) {
                    console.log(response.errors);
                    notification['error']({
                        message: intl.formatMessage({id: "error.occured"})
                    })
                } else {
                    notification['success']({
                        message: intl.formatMessage({id: "category.update.success"})
                    });
                    router.push(`/${DASHBOARD_PATH}ecommerce/categories`)
                }
            })
        }
    }

	return (
		<Widget styleName="gx-card-profile">
            <div className="ant-card-head">
                <span className="ant-card-head-title gx-mb-2">
                    {category?
                        <IntlMessages id="categories.form.edit_title"/>
                    :
                        <IntlMessages id="categories.form.create_title"/>
                    }
                </span>
            </div>
            <Form 
                layout="vertical" 
                hideRequiredMark
                onFinish={onSubmitForm}
                initialValues={{
                    ...category
                }}
            >
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="name"
							label={intl.formatMessage({ id: "form.name" })}
							rules={[{
								required: true,
								message: intl.formatMessage({ id: "validation.field.required" })
							}]}
						>
							<Input/>
						</Form.Item>
					</Col>
				</Row>
				<Row gutter={16}>
					<Col span={24}>
						<Form.Item
							name="description"
							label={intl.formatMessage({ id: "form.description" })}
						>
							<Input.TextArea rows={4}/>
						</Form.Item>
					</Col>
				</Row>
                <Row gutter={16}>
					<Col span={24}>
                        <Form.Item>
                            <Button type="primary" htmlType="submit" style={{float: "right"}}>
                                <IntlMessages id="button.submit"/> 
                            </Button>
                        </Form.Item>
					</Col>
				</Row>
			</Form>
		</Widget>
	);
};

export default CategoryForm;
