import '../public/css/bootstrap.min.css';
import '../public/css/fontawesome.min.css';
import '../public/css/animate.min.css';
import '../public/css/flaticon.css';
import '../node_modules/react-modal-video/css/modal-video.min.css';
import 'react-image-lightbox/style.css';
import '../public/css/style.css';
import '../public/css/responsive.css';

import React from 'react';
import Head from 'next/head'
import withRedux from 'next-redux-wrapper';
import 'antd/dist/antd.css';
import "../public/vendors/style";
import "../styles/style.css"

import initStore from '../redux/store';
import {Provider} from "react-redux";
import LocaleProvider from "../app/core/LocaleProvider";
import {AuthProvider} from "../util/use-auth";
import Layout from "../app/core/Layout";
import { useRouter } from 'next/router';

const Page = ({Component, pageProps, store}) => {
  const router = useRouter();
  
  return (
    <>
      <Head>
        <meta charSet="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />

        <title>ATR Company</title>
      </Head>
      <Provider store={store}>
        <LocaleProvider>
            <AuthProvider>
              <Layout>
                <Component {...pageProps} />
              </Layout>
            </AuthProvider>
          
        </LocaleProvider>
      </Provider>
    </>
  );
};

export default withRedux(initStore)(Page);

{/*  */}