import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { CategoriesApi, EmployeesApi } from '../../../api';

const EmployeeForm = asyncComponent(() => import('../../../routes/Accounts/Employees/EmployeeForm'));

const EditEmployee = (props) => <EmployeeForm props={props}/>;

export default EditEmployee;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const employee = await EmployeesApi.get({
        _id: context.query.eid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {employee: employee?employee:null}}
}