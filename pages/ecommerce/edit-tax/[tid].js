import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { TaxesApi } from '../../../api';

const TaxForm = asyncComponent(() => import('../../../routes/Ecommerce/Taxes/TaxForm'));

const EditTax = (props) => <TaxForm props={props}/>;

export default EditTax;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const tax = await TaxesApi.get({
        _id: context.query.tid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {tax: tax?tax:null}}
}