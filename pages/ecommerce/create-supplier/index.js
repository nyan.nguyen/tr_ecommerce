import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const SupplierForm = asyncComponent(() => import('../../../routes/Ecommerce/Suppliers/SupplierForm'));

const CreateSupplier = () => <SupplierForm/>;

export default CreateSupplier;