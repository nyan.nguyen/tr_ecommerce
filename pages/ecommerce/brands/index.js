import React from 'react';
import { BrandsApi } from '../../../api';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';

const BrandsComponent = asyncComponent(() => import('../../../routes/Ecommerce/Brands'));

const Brands = (props) => <BrandsComponent props={props}/>;

export default Brands;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const brands = await BrandsApi.getBrands({
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {brands: brands?brands:[]}}
}