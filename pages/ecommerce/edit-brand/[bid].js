import React from 'react';
import asyncComponent from '../../../util/asyncComponent'
import cookie from 'cookie';
import { BrandsApi } from '../../../api';

const BrandForm = asyncComponent(() => import('../../../routes/Ecommerce/Brands/BrandForm'));

const EditBrand = (props) => <BrandForm props={props}/>;

export default EditBrand;

export async function getServerSideProps(context) {
    const cookies =  context.req.headers.cookie?cookie.parse(context.req.headers.cookie):"";
    const brand = await BrandsApi.getBrand({
        bid: context.query.bid
    },{
        headers: {
            "x-auth-token" :cookies.token
        }
    });
    
    return {props: {brand: brand?brand:null}}
}