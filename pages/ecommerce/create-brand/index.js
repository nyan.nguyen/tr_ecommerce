import React from 'react';
import asyncComponent from '../../../util/asyncComponent'

const BrandForm = asyncComponent(() => import('../../../routes/Ecommerce/Brands/BrandForm'));

const CreateBrand = () => <BrandForm/>;

export default CreateBrand;