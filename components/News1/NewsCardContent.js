import React from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import { Image } from 'antd';

const NewsCardContent = ({props}) => {
  const {products} = props;

  const { t } = useTranslation('common');
  return (
    <>
      <section className="blog-area ptb-100">
        <div className="container">
          <div className="row">
            {products.map(product => {
              return(<div className="col-lg-4 col-md-6">
              <div className="single-blog-post text-center">
                <Image width={300} src={product.cover}/>

                <div className="post-content">
                  <h3>
                    <Link href={`/product/${product._id}`}>
                      <a>{product.name}</a>
                    </Link>
                  </h3>
                  <p>{product.short_description}</p>
                </div>
              </div>
            </div>
            )
            })}
            {/* Pagination */}
            {/* <div className="col-lg-12 col-md-12">
                                <div className="pagination-area">
                                    <Link href="#">
                                        <a className="prev page-numbers">
                                            <i className="fas fa-angle-double-left"></i>
                                        </a>
                                    </Link>
                                    <Link href="#">
                                        <a className="page-numbers">1</a>
                                    </Link>
                                    <span className="page-numbers current" aria-current="page">2</span>
                                    <Link href="#">
                                        <a className="page-numbers">3</a>
                                    </Link>
                                    <Link href="#">
                                        <a className="page-numbers">4</a>
                                    </Link>
                                    <Link href="#">
                                        <a className="next page-numbers">
                                            <i className="fas fa-angle-double-right"></i>
                                        </a>
                                    </Link>
                                </div>
                            </div> */}
            {/* End Pagination */}
          </div>
        </div>
      </section>
    </>
  );

}

export default NewsCardContent;