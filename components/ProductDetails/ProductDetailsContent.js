import React, { Component, useState, useEffect } from 'react';
import Link from 'next/link';
import { useTranslation } from 'next-i18next';
import Colors from './Colors';
import DetailsThumb from './DetailsThumb';
import { Image } from 'antd';

const ProductDetailsContent = ({props}) => {
    const {product, variations} = props;
    const { t } = useTranslation('common');
    const [index, setIndex] = useState(0);
    const [mainPhoto, setMainPhoto] = useState("");

    const myRef = React.createRef();

    const handleTab = img => {
        setMainPhoto(img);
    }

    useEffect(() => {
        if(product) setMainPhoto(product.cover);
    },[product])

    useEffect(() => {
        // myRef.current.children[index].className = "active";
    }, []);

    return (
        <>
            <section className="event-details-area ptb-100">
                <div className="container">
                    <div className="row">
                        <div className="event-details">
                            <div className="event-details-header">
                                <Link href="/products">
                                    <a className="back-all-event">
                                        <i className="flaticon-left-chevron"></i> {t('ProductDetail.2')}
                                            </a>
                                </Link>
                            </div>

                            <div className="product-detail">
                                <div className="details" key={product._id}>
                                    <Image width={500} src={mainPhoto}/>

                                    <div className="box">
                                        <div className="row">
                                            <h2>{product.name}</h2>
                                        </div>

                                        {product?.variants?.map((variation,idx) => {
                                            return variation.attributeGroups.map((attributeGroup,idx) => {
                                                let group = variations.find(v => v._id === attributeGroup.split("#")[0]);
                                                let value = group?group.values.find(v => v._id === attributeGroup.split("#")[1]):{};
                                                if(group.name.toLowerCase() === "colors") {
                                                    return <div className="colors d-inline-block">
                                                        <button onClick={() => (variation.images.length && product.galery.length)?handleTab(product.galery[variation.images[0]]):null} style={{background: value.value}} key={value._id}></button>
                                                    </div>
                                                }
                                            })
                                        })}

                                        {/* <div className="colors">
                                            <button style={{background: color}} key={index}></button>
                                        </div> */}

                                        {/* <h5>{item.description}</h5> */}
                                        <p>{product.short_description}</p>

                                        <DetailsThumb images={product.galery} tab={handleTab} myRef={myRef} />

                                    </div>
                                </div>
                            </div>

                          
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export default ProductDetailsContent;